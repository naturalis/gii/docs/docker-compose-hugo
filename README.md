# Hugo Documentation server for Infra

This docker-compose configuration installs Traefik and a pre-fabricted container _with_ current content.
As for now we use _Basic Authentication_ . After every commit in the infra-documentation, content validity will be tested, and a new container with web-content will be deployed.

# Deployment

```bash
cd gii/docs/ansible-docs
ansible-galaxy install -r ./requirements.yml --roles-path ./roles/
ansible-playbook --vault-password-file ~/.vault_dc_delft playbooks/deploy.yml -l docs-infra-prd-001 
```
## Extra actions
Apart from Traefik and Route53 actions:
- create a gitlab read-only-token to the container registry, see [here](https://gitlab.com/naturalis/lib/ansible/ansible-role-docker-compose/-/blob/master/README.md)
- install a crontab to pull updated container every 15 minutes.

Both taken care of by ansible.

# Flow after new content is commited.
- After a commit in the core/infra-documentation repo, a gitlab-runner will stage a test for hugo-content validity.
- If this succeeds, a container will be will be build containing hugo-converted web-content.
- This container will be published to the registry.
- A crontab will check with a 'docker-compose pull' if a new container is availble
- If so, this one will be pulled and replace the running one.

# Authentication / Authorization
To protect our documentation we need some authorization. Best would bij Google-Naturalis authentication limited to Infra members. This is ToDo.
For now we go for the simple 'Basic Authentication' with the user _ictdocs_. See Bitwarden

